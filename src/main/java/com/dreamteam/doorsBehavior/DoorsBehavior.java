package com.dreamteam.doorsBehavior;

public interface DoorsBehavior {
    void openDoors();
}

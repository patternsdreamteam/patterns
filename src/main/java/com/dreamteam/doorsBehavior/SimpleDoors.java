package com.dreamteam.doorsBehavior;

public class SimpleDoors implements DoorsBehavior {
    @Override
    public void openDoors() {
        System.out.println("ok ... simple... the main thing is not to walk!");
    }
}

package com.dreamteam.cars;

import com.dreamteam.doorsBehavior.DoorsBehavior;
import com.dreamteam.fuel.FuelBehavior;
import com.dreamteam.transmission.TransmissionBehavior;

public class SportCar extends Car {
    public SportCar(String name, float price, TransmissionBehavior transmissionBehavior, FuelBehavior fuelBehavior, DoorsBehavior doorsBehavior) {
        super(name, price, transmissionBehavior, fuelBehavior, doorsBehavior);
    }
}
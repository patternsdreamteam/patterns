package com.dreamteam.cars;

import com.dreamteam.doorsBehavior.DoorsBehavior;
import com.dreamteam.transmission.TransmissionBehavior;
import com.dreamteam.fuel.FuelBehavior;

public abstract class Car {
    protected String name;
    protected float price;
    protected TransmissionBehavior transmissionBehavior;
    protected DoorsBehavior doorsBehavior;
    protected FuelBehavior fuelBehavior;

    protected Car(String name, float price, TransmissionBehavior transmissionBehavior, FuelBehavior fuelBehavior, DoorsBehavior doorsBehavior) {
        this.name = name;
        this.price = price;
        this.doorsBehavior = doorsBehavior;
        this.transmissionBehavior = transmissionBehavior;
        this.fuelBehavior = fuelBehavior;
    }

    public void setTransmissionBehavior(TransmissionBehavior transmissionBehavior) {
        this.transmissionBehavior = transmissionBehavior;
    }

    public void setDoorsBehavior(DoorsBehavior doorsBehavior) {
        this.doorsBehavior = doorsBehavior;
    }

    public void setFuelBehavior(FuelBehavior fuelBehavior) {
        this.fuelBehavior = fuelBehavior;
    }

    public void switchValue() {
        transmissionBehavior.switchValue();
    }

    public void use() {
        fuelBehavior.use();
    }

    public void openDoors() {
        doorsBehavior.openDoors();
    }
}
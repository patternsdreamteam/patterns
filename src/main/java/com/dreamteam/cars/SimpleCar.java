package com.dreamteam.cars;

import com.dreamteam.doorsBehavior.DoorsBehavior;
import com.dreamteam.transmission.TransmissionBehavior;
import com.dreamteam.fuel.FuelBehavior;

public class SimpleCar extends Car {
    public SimpleCar(String name, float price, TransmissionBehavior transmissionBehavior, FuelBehavior fuelBehavior, DoorsBehavior doorsBehavior) {
        super(name, price, transmissionBehavior, fuelBehavior, doorsBehavior);
    }
}
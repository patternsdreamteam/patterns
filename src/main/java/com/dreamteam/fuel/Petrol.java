package com.dreamteam.fuel;

public class Petrol implements FuelBehavior {
    @Override
    public void use() {
        System.out.println("We switched to petrol. Now car is using petrol as a fuelBehavior");
    }
}
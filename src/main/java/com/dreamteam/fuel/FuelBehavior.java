package com.dreamteam.fuel;

public interface FuelBehavior {
    void use();
}
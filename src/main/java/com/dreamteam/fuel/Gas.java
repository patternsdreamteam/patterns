package com.dreamteam.fuel;

public class Gas implements FuelBehavior {
    @Override
    public void use() {
        System.out.println("We switched to gas. Now car is using gas as a fuelBehavior");
    }
}
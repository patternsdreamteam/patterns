package com.dreamteam.factory;

import com.dreamteam.cars.SimpleCar;
import com.dreamteam.cars.SportCar;
import com.dreamteam.doorsBehavior.ScissorDoors;
import com.dreamteam.doorsBehavior.SimpleDoors;
import com.dreamteam.transmission.AutoTransmission;
import com.dreamteam.transmission.ManualTransmission;
import com.dreamteam.fuel.Gas;
import com.dreamteam.fuel.Petrol;

public class BMWFactory implements CarAbstractFactory {
    @Override
    public SportCar createSportCar() {
        return new SportCar("BMW Z4", 300000, new AutoTransmission(), new Gas(), new ScissorDoors());
    }

    @Override
    public SimpleCar createSimpleCar() {
        return new SimpleCar("BMW M3", 30000, new ManualTransmission(), new Petrol(), new SimpleDoors());
    }
}
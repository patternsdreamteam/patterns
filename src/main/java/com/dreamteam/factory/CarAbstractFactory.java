package com.dreamteam.factory;

import com.dreamteam.cars.SimpleCar;
import com.dreamteam.cars.SportCar;

public interface CarAbstractFactory {
    SportCar createSportCar();
    SimpleCar createSimpleCar();
}
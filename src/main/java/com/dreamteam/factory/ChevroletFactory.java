package com.dreamteam.factory;

import com.dreamteam.cars.SimpleCar;
import com.dreamteam.cars.SportCar;
import com.dreamteam.doorsBehavior.ScissorDoors;
import com.dreamteam.doorsBehavior.SimpleDoors;
import com.dreamteam.fuel.Gas;
import com.dreamteam.fuel.Petrol;
import com.dreamteam.transmission.AutoTransmission;
import com.dreamteam.transmission.ManualTransmission;

public class ChevroletFactory implements CarAbstractFactory {
    @Override
    public SportCar createSportCar() {
        return new SportCar("Chevrolet Camaro SS", 200000, new AutoTransmission(), new Petrol(), new ScissorDoors());
    }

    @Override
    public SimpleCar createSimpleCar() {
        return new SimpleCar("Chevrolet Aveo", 20000, new ManualTransmission(), new Gas(), new SimpleDoors());
    }
}
package com.dreamteam.autostore;

import com.dreamteam.cars.Car;

import java.util.LinkedList;
import java.util.List;

public class AutoStore {
    List<Car> list;

    public AutoStore() {
        list = new LinkedList<>();
    }

    public void add(Car newCar) {
        list.add(newCar);
    }

    public Iterator getIterator() {
        return new CarIterator(list);
    }
}
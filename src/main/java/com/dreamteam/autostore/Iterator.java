package com.dreamteam.autostore;

public interface Iterator {
    boolean hasNext();
    Object next();
}
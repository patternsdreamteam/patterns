package com.dreamteam.autostore;

import com.dreamteam.cars.Car;

import java.util.List;

public class CarIterator implements Iterator {
    List<Car> list;
    int position;

    public CarIterator(List<Car> list) {
        this.list = list;
        position = 0;
    }

    @Override
    public boolean hasNext() {
        return position < list.size();
    }

    @Override
    public Car next() {
        position++;
        return list.get(position - 1);
    }
}